
public class Triangle {
    private int a, b, c;

    public Triangle(int a, int b, int c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public void areaTriangle() {
        int pyarea = (a + b + c) / 2;
        double root = Math.sqrt(8 * ((8 - a) * (8 - b) * (8 - c)));
        System.out.println("Area of Triangle(90) :" + pyarea);
        System.out.println("Area of Triangle(Pyramid) : " + root);
    }

    public void printPerimeter() {
        double perimeterTriangle = (a * a) + (b * b) - (2 * a * b) * (Math.cos(c));
        System.out.println("Perimeter of Triangle : " + Math.sqrt(perimeterTriangle));

    }
}
